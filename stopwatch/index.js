let startBtn = document.getElementById("start");
let stopBtn = document.getElementById("stop");
let resetBtn = document.getElementById("reset");
let display = document.querySelector(".timeDisplay");

 
// Initialization
 
let ms = 00;
let s = 00;
let min = 00;
let hr = 00;
let timeStorage=null;




//button clickable

startBtn.addEventListener('click', ()=>{
    if(timeStorage!==null){
        clearInterval(timeStorage);
    }
    timeStorage = setInterval(startTime,10);
});

stopBtn.addEventListener('click', ()=>{
    clearInterval(timeStorage);
});

resetBtn.addEventListener('click', ()=>{
    clearInterval(timeStorage);
    [ms,s,min,hr] = [0,0,0,0];
    display.innerHTML = '00:00:00:00';
});




// Process on how the counting works
const startTime = () => {
    ms++;
    if (ms == 100) {
        ms = 0;
        s++;
    }
 
    if (s == 60) {
        s = 0;
        min++;
    } if (min == 60) {
        min = 0;
        hr++;
    }
 
    stringMin = min.toString().padStart(2, "0");
    stringSec = s.toString().padStart(2, "0");
    stringMs = ms.toString().padStart(2, "0");
    stringHr = hr.toString().padStart(2, "00");
 
    display.innerHTML = `${stringHr}:${stringMin}:${stringSec}:${stringMs}`;
 
}
